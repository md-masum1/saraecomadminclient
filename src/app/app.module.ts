import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { SpinnerService } from './shared/service/spinner.service';

import { AppComponent } from './app.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({
      config: {
         tokenGetter,
        //  whitelistedDomains: ['192.168.2.76'],
        //  blacklistedRoutes: ['192.168.2.76/api/auth']
        whitelistedDomains: ['localhost:55058'],
        blacklistedRoutes: ['localhost:55058/api/auth']
      }
   }),
  ],
  providers: [
    SpinnerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
