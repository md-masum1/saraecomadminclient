import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ContentLayoutComponent } from './shared/layout/content-layout/content-layout.component';
import { AuthGuard } from './_guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./components/dashboard/dashboard.module').then(
            m => m.DashboardModule
          )
      },
      {
        path: 'products',
        loadChildren: () =>
          import('./components/products/products.module').then(
            m => m.ProductsModule
          ),
        data: {
          breadcrumb: 'Products'
        }
      },
      {
        path: 'setup',
        loadChildren: () =>
          import('./components/setup/setup.module').then(m => m.SetupModule),
        data: {
          breadcrumb: 'Setup'
        }
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('./components/admin/admin.module').then(m => m.UsersModule),
        data: {
          breadcrumb: 'Admin'
        }
      },
      {
        path: 'menus',
        loadChildren: () =>
          import('./components/menus/menus.module').then(m => m.MenusModule),
        data: {
          breadcrumb: 'Menus'
        }
      },

    ]
  },

  {
    path: 'auth',
    loadChildren: () =>
      import('./components/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
