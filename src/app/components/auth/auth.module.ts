import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { AuthService } from './service/auth.service';
import { AuthRoutingModule } from './auth-routing.module';

import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    AuthRoutingModule,
    SharedModule
  ],
  providers: [
    AuthService
  ]
})
export class AuthModule { }
