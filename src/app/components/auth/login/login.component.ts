import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/components/auth/service/auth.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  model: any = {};

  owlcarousel = [
    {
      title: 'Welcome to SaRa Lifestyle Ltd',
      desc:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.'
    },
    {
      title: 'Welcome to SaRa Lifestyle Ltd',
      desc:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.'
    },
    {
      title: 'Welcome to SaRa Lifestyle Ltd',
      desc:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.'
    }
  ];
  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };

  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    private spinner: SpinnerService
  ) {}

  ngOnInit() {
    if (this.authService.loggedIn()) {
      this.router.navigate(['/dashboard/default/']);
    }
    this.createLoginForm();
  }

  login() {
    this.spinner.showSpinner();
    this.authService.login(this.loginForm.value).subscribe(
      next => {
        this.alertify.success('Login successfully!');
      },
      error => {
        this.alertify.error('Failed to login!');
        this.spinner.hideSpinner();
      },
      () => {
        this.spinner.hideSpinner();
        this.router.navigate(['/dashboard/default/']);
      }
    );
  }

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      userName: [''],
      password: ['']
    });
  }
}
