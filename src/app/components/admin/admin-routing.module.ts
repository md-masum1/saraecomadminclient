import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserComponent } from './user-management/list-user/list-user.component';
import { CreateUserComponent } from './user-management/create-user/create-user.component';

import { ListUserRolesComponent } from './role-management/list-user-roles/list-user-roles.component';
import { RolesModalComponent } from './role-management/roles-modal/roles-modal.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user-management/list-user',
        component: ListUserComponent,
        data: {
          title: 'User List',
          breadcrumb: 'User List'
        }
      },
      {
        path: 'user-management/create-user',
        component: CreateUserComponent,
        data: {
          title: 'Create User',
          breadcrumb: 'Create User'
        }
      },
      {
        path: 'role-management/list-user-roles',
        component: ListUserRolesComponent,
        data: {
          title: 'User List With Role',
          breadcrumb: 'User Role'
        }
      },
      {
        path: 'role-management/roles-modal',
        component: RolesModalComponent,
        data: {
          title: 'User Role',
          breadcrumb: 'User Role'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
