export interface User {
    id: number;
    userName: string;
    email: string;
    employeeId: string;
    password: string;
    roles?: string[];
}
