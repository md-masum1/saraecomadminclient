import { NgModule } from '@angular/core';
import { ModalModule, PaginationModule } from 'ngx-bootstrap';

import { SharedModule } from '../../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';

import { ListUserComponent } from './user-management/list-user/list-user.component';
import { CreateUserComponent } from './user-management/create-user/create-user.component';
import { ListUserRolesComponent } from './role-management/list-user-roles/list-user-roles.component';
import { RolesModalComponent } from './role-management/roles-modal/roles-modal.component'

@NgModule({
  declarations: [
    ListUserComponent,
    CreateUserComponent,
    ListUserRolesComponent,
    RolesModalComponent
  ],
  imports: [
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    AdminRoutingModule,
    SharedModule
  ],
  entryComponents: [
    CreateUserComponent,
    RolesModalComponent
  ],
})
export class UsersModule { }
