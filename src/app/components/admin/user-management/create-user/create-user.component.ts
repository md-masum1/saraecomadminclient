import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from 'src/app/components/admin/service/admin.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { BsModalRef } from 'ngx-bootstrap';
import { User } from 'src/app/components/admin/model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  @Output() userToCreate = new EventEmitter();
  employeeForm: FormGroup;
  user: User;
  spinnerName = 'createSpinner';

  constructor(private adminService: AdminService,
              private fb: FormBuilder,
              private spinner: SpinnerService,
              private alertify: AlertifyService,
              private router: Router,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createEmployeeForm();
  }

  createEmployeeForm() {
    this.employeeForm = this.fb.group({
      userName: ['', Validators.required],
      email: ['', Validators.required],
      employeeId: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.userToCreate.emit(this.employeeForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.employeeForm.reset();
  }

}
