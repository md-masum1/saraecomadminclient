import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from 'src/app/components/admin/service/admin.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { User } from 'src/app/components/admin/model/user';
import { PaginatedResult, Pagination } from 'src/app/_models/pagination';

import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { CreateUserComponent } from '../create-user/create-user.component';



@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users: User[];
  pagination: Pagination;
  spinnerName = 'listSpinner';
  currentPage = 1;
  bsModalRef: BsModalRef;

  constructor(private adminService: AdminService,
              private spinner: SpinnerService,
              private alertify: AlertifyService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.spinner.showSpinner(this.spinnerName);
    this.adminService.getAllUsers().subscribe((data: PaginatedResult<User[]>) => {
      this.users = data.result;
      this.pagination = data.pagination;
      this.currentPage = this.pagination.currentPage;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load user list');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  getAllUser() {
    this.spinner.showSpinner(this.spinnerName);

    this.adminService.getAllUsers(this.pagination.currentPage, this.pagination.itemsPerPage).subscribe((data: PaginatedResult<User[]>) => {
      this.users = data.result;
      this.pagination = data.pagination;
      this.currentPage = this.pagination.currentPage;
    }, error => {
      this.alertify.error('Failed to load user list');
      this.spinner.hideSpinner(this.spinnerName);
      console.log(error);
    }, () => {
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createdUser(user: User) {
    // this.users.push(user);
    this.getAllUser();
  }

  deleteUser(userId: number) {
    this.alertify.confirm('Are you sure you want to delete this user', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.adminService.deleteUser(userId).subscribe(() => {
        this.alertify.success('user deleted successfully');
        // this.users.splice(this.users.findIndex(u => u.id === userId));
        this.spinner.hideSpinner(this.spinnerName);
        this.getAllUser();
      }, error => {
        this.spinner.hideSpinner(this.spinnerName);
        this.alertify.error('Failed to delete user');
        console.log(error);
      });
    });
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.currentPage = this.pagination.currentPage;
    this.getAllUser();
  }

  createEmployeeForm() {
    this.bsModalRef = this.modalService.show(CreateUserComponent,  {
      initialState: {
        title: 'Create User',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.userToCreate.subscribe((user: User) => {
      const userToCreate = user;
      console.log(userToCreate);
      if (userToCreate) {
        userToCreate.id = 0;
        this.adminService.createUser(userToCreate).subscribe(() => {
          this.alertify.success('User created successfully !');
          // this.loadUser();
        }, error => {
          this.alertify.error('Failed to create menu !');
          console.log(error);
        });
      }
    });
  }

}
