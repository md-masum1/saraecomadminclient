import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { User } from 'src/app/components/admin/model/user';
import { AdminService } from 'src/app/components/admin/service/admin.service';
import { RolesModalComponent } from '../roles-modal/roles-modal.component';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';




@Component({
  selector: 'app-list-user-roles',
  templateUrl: './list-user-roles.component.html',
  styleUrls: ['./list-user-roles.component.css']
})
export class ListUserRolesComponent implements OnInit {
  users: User[];
  bsModalRef: BsModalRef;
  allRoles: any [];

  constructor(private adminService: AdminService,
              private modalService: BsModalService,
              private spinner: SpinnerService,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.spinner.showSpinner();
    this.adminService.getAllRoles().subscribe((data: any[]) => {
      this.allRoles = data;
    }, error => {
      this.alertify.error('Failed to load roles');
    }, () => {
      this.spinner.hideSpinner();
    });
    this.getUserWithRoles();
  }

  getUserWithRoles() {
    this.spinner.showSpinner();
    this.adminService.getUserWithRoles().subscribe((users: User[]) => {
      this.users = users;
      this.spinner.hideSpinner();
    }, error => {
      console.log('error', error);
    });
  }

  editRolesModal(user: User) {
    const initialState = {
      user,
      roles: this.getRolesArray(user)
    };
    this.bsModalRef = this.modalService.show(RolesModalComponent, {
      initialState,
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });
    this.bsModalRef.content.updateSelectedRoles.subscribe((values) => {
      const roleToUpdate = {
        roleNames: [...values.filter(el => el.checked === true).map(el => el.name)]
      };
      if (roleToUpdate) {
        this.spinner.showSpinner();
        this.adminService.updateUserRoles(user, roleToUpdate).subscribe(() => {
          user.roles = [...roleToUpdate.roleNames];
          this.spinner.hideSpinner();
        }, error => {
          console.log(error);
        });
      }
    });
  }

  private getRolesArray(user) {
    const roles = [];
    const userRoles = user.roles;
    const availableRoles: any[] = this.allRoles;

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < availableRoles.length; i++) {
      let isMatch = false;
      // tslint:disable-next-line: prefer-for-of
      for (let j = 0; j < userRoles.length; j++) {
        if (availableRoles[i].name === userRoles[j]) {
          isMatch = true;
          availableRoles[i].checked = true;
          roles.push(availableRoles[i]);
          break;
        }
      }
      if (!isMatch) {
        availableRoles[i].checked = false;
        roles.push(availableRoles[i]);
      }
    }

    return roles;
  }


}
