import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/components/admin/model/user';
import { BsModalRef } from 'ngx-bootstrap/modal';



@Component({
  selector: 'app-roles-modal',
  templateUrl: './roles-modal.component.html',
  styleUrls: ['./roles-modal.component.css']
})
export class RolesModalComponent implements OnInit {
  @Output() updateSelectedRoles = new EventEmitter();
  user: User;
  roles: any[];

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
    console.log(this.user);
    console.log(this.roles);
  }

  updateRoles() {
    this.updateSelectedRoles.emit(this.roles);
    this.bsModalRef.hide();
  }

}
