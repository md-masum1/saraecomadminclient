import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../model/user';
import { PaginatedResult } from '../../../_models/pagination';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

createUser(user: User) {
  return this.http.post(this.baseUrl + 'admin/createUser/', user);
}

getUserWithRoles() {
  return this.http.get(this.baseUrl + 'admin/userWithRoles');
}

getAllRoles() {
  return this.http.get(this.baseUrl + 'admin/roles');
}

updateUserRoles(user: User, roles: {}) {
  return this.http.post(this.baseUrl + 'admin/editRoles/' + user.userName, roles);
}

getAllUsers(page?, itemsPerPage?): Observable<PaginatedResult<User[]>> {
  const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();

  let params = new HttpParams();

  if (page != null && itemsPerPage != null) {
    params = params.append('pageNumber', page);
    params = params.append('pageSize', itemsPerPage);
  }

  return this.http.get<User[]>(this.baseUrl + 'admin/users', { observe: 'response', params })
  .pipe(
    map(response => {
      paginatedResult.result = response.body;
      if (response.headers.get('Pagination') != null) {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
      }
      return paginatedResult;
    })
  );
}

deleteUser(userId: number) {
  return this.http.post(this.baseUrl + 'admin/deleteUser/' + userId, null);
}
}
