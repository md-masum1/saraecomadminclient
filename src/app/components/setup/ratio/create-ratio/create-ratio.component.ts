import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Ratio } from 'src/app/components/setup/model/ratio';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';

@Component({
  selector: 'app-create-ratio',
  templateUrl: './create-ratio.component.html',
  styleUrls: ['./create-ratio.component.css']
})
export class CreateRatioComponent implements OnInit {

  ratioForm: FormGroup;
  ratio: Ratio;
  spinnerName = 'createSpinner';
  title: string;
  @Output() ratioToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.createRatioForm();
  }

  createRatioForm() {
    if (this.ratio) {
      this.ratioForm = this.fb.group({
        id: [this.ratio.id],
        name: [this.ratio.name, Validators.required]
      });
    } else {
      this.ratioForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.ratioToCreate.emit(this.ratioForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.ratioForm.reset();
  }

}
