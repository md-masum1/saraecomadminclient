import { Component, OnInit } from '@angular/core';
import { RatioService } from 'src/app/components/setup/service/ratio.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Ratio } from 'src/app/components/setup/model/ratio';
import { environment } from 'src/environments/environment';
import { CreateRatioComponent } from '../create-ratio/create-ratio.component';

@Component({
  selector: 'app-list-ratio',
  templateUrl: './list-ratio.component.html',
  styleUrls: ['./list-ratio.component.css']
})
export class ListRatioComponent implements OnInit {

  spinnerName = 'listSpinner';
  ratios: Ratio[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;

  constructor(private ratioService: RatioService,
              private alertify: AlertifyService,
              private router: Router,
              private spinner: SpinnerService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.loadRatio();
  }

  loadRatio() {
    this.spinner.showSpinner(this.spinnerName);
    this.ratioService.getRatios().subscribe((data: Ratio[]) => {
      this.ratios = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load ratio !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createRatio() {
    this.bsModalRef = this.modalService.show(CreateRatioComponent,  {
      initialState: {
        title: 'Create/Update Ratio',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.ratioToCreate.subscribe((ratio: Ratio) => {
      const ratioToCreate = ratio;
      if (ratioToCreate) {
        ratioToCreate.id = 0;
        this.ratioService.createRatio(ratioToCreate).subscribe(() => {
          this.alertify.success('Ratio created successfully');
          this.loadRatio();
        }, error => {
          this.alertify.error('Failed to create ratio!');
          console.log(error);
        });
      }
    });
  }

  editRatio(ratio) {

  }

  deleteRatio(id: number) {

  }
}

