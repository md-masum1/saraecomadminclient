import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Banner } from 'src/app/components/setup/model/banner';
import { environment } from 'src/environments/environment';
import { BsModalRef } from 'ngx-bootstrap';



@Component({
  selector: 'app-create-banner',
  templateUrl: './create-banner.component.html',
  styleUrls: ['./create-banner.component.css']
})
export class CreateBannerComponent implements OnInit {

  bannerForm: FormGroup;
  banner: Banner;
  spinnerName = 'createSpinner';
  title: string;
  fileUrl = environment.fileUrl;
  @Output() bannerToCreate = new EventEmitter();
  @ViewChild('fileInput', {static: false}) fileInputVariable: ElementRef;

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createBannerForm();
  }

  createBannerForm() {
    if (this.banner) {
      this.bannerForm = this.fb.group({
        id: [this.banner.id],
        title: [this.banner.title, Validators.required],
        description: [this.banner.description, Validators.required],
        navigateUrl: [this.banner.navigateUrl, Validators.required],
        displayOrder: [this.banner.displayOrder, Validators.required],
        imageFile: [null]
      });
    } else {
      this.bannerForm = this.fb.group({
        id: [0],
        title: [null, Validators.required],
        description: [null, Validators.required],
        navigateUrl: [null, Validators.required],
        displayOrder: [null, Validators.required],
        imageFile: [null, Validators.required]
      });
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bannerForm.get('imageFile').setValue(file);
    }
  }

  onSubmit(): void {
    const formModel = this.prepareSave();
    this.bannerToCreate.emit(formModel);
    this.bsModalRef.hide();
  }

  private prepareSave(): any {
    const input = new FormData();
    input.append('id', this.bannerForm.get('id').value);
    input.append('title', this.bannerForm.get('title').value);
    input.append('description', this.bannerForm.get('description').value);
    input.append('navigateUrl', this.bannerForm.get('navigateUrl').value);
    input.append('displayOrder', this.bannerForm.get('displayOrder').value);
    input.append('imageFile', this.bannerForm.get('imageFile').value);
    return input;
  }

}
