import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { Banner } from 'src/app/components/setup/model/banner';
import { BannerService } from 'src/app/components/setup/service/banner.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { CreateBannerComponent } from '../create-banner/create-banner.component';

@Component({
  selector: 'app-list-banner',
  templateUrl: './list-banner.component.html',
  styleUrls: ['./list-banner.component.css']
})
export class ListBannerComponent implements OnInit {
  spinnerName = 'listSpinner';
  banners: Banner[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;
  constructor(
    private bannerService: BannerService,
    private alertify: AlertifyService,
    private spinner: SpinnerService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.getBanners();
  }

  getBanners() {
    this.spinner.showSpinner(this.spinnerName);
    this.bannerService.getBanners().subscribe((banner: Banner[]) => {
      this.banners = banner;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load banner !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  deleteBanner(id: number) {
    this.alertify.confirm('Are you sure you want to delete this banner???', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.bannerService.deleteBanner(id).subscribe(() => {
        this.alertify.success('banner delete successfully');
        this.getBanners();
        this.spinner.hideSpinner(this.spinnerName);
      }, error => {
        this.alertify.error('failed to delete banner');
        this.spinner.hideSpinner(this.spinnerName);
      });
    });
  }

  createBanner() {
    this.bsModalRef = this.modalService.show(CreateBannerComponent,  {
      initialState: {
        title: 'Create HomePage Banner'
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.bannerToCreate.subscribe((banner: Banner) => {
      const bannerToCreate = banner;
      if (bannerToCreate) {
        bannerToCreate.id = 0;
        this.bannerService.postBanner(bannerToCreate).subscribe(() => {
          this.alertify.success('banner create successfully');
          this.getBanners();
        }, error => {
          this.alertify.error('Failed to create banner');
          console.log(error);
        });
      }
    });
  }

  updateBanner(banner: Banner) {
    this.bsModalRef = this.modalService.show(CreateBannerComponent,  {
      initialState: {
        title: 'Update HomePage Banner',
        banner
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.bannerToCreate.subscribe((bannerToUpdate: Banner) => {
      const bannerToCreate = bannerToUpdate;
      if (bannerToCreate) {
        bannerToCreate.id = banner.id;
        this.bannerService.postBanner(bannerToCreate).subscribe(() => {
          this.alertify.success('banner update successfully');
          this.getBanners();
        }, error => {
          this.alertify.error('Failed to update banner');
          console.log(error);
        });
      }
    });
  }

}
