import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Slider } from 'src/app/components/setup/model/slider';
import { environment } from 'src/environments/environment';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-slider',
  templateUrl: './create-slider.component.html',
  styleUrls: ['./create-slider.component.css']
})
export class CreateSliderComponent implements OnInit {


  sliderForm: FormGroup;
  slider: Slider;
  spinnerName = 'createSpinner';
  title: string;
  fileUrl = environment.fileUrl;
  @Output() sliderToCreate = new EventEmitter();
  @ViewChild('fileInput', {static: false}) fileInputVariable: ElementRef;

  constructor(private fb: FormBuilder,
    public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createSliderForm();
  }

  createSliderForm() {
    if (this.slider) {
      this.sliderForm = this.fb.group({
        id: [this.slider.id],
        title: [this.slider.title, Validators.required],
        description: [this.slider.description, Validators.required],
        navigateUrl: [this.slider.navigateUrl, Validators.required],
        displayOrder: [this.slider.displayOrder, Validators.required],
        imageFile: [null]
      });
    } else {
      this.sliderForm = this.fb.group({
        id: [0],
        title: [null, Validators.required],
        description: [null, Validators.required],
        navigateUrl: [null, Validators.required],
        displayOrder: [null, Validators.required],
        imageFile: [null, Validators.required]
      });
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.sliderForm.get('imageFile').setValue(file);
    }
  }

  onSubmit(): void {
    const formModel = this.prepareSave();
    this.sliderToCreate.emit(formModel);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.sliderForm.reset();
    this.fileInputVariable.nativeElement.value = '';
  }

  private prepareSave(): any {
    const input = new FormData();
    input.append('id', this.sliderForm.get('id').value);
    input.append('title', this.sliderForm.get('title').value);
    input.append('description', this.sliderForm.get('description').value);
    input.append('navigateUrl', this.sliderForm.get('navigateUrl').value);
    input.append('displayOrder', this.sliderForm.get('displayOrder').value);
    input.append('imageFile', this.sliderForm.get('imageFile').value);
    return input;
  }


}
