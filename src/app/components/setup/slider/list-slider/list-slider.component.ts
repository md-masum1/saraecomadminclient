import { Component, OnInit } from '@angular/core';
import { SliderService } from 'src/app/components/setup/service/slider.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { Slider } from 'src/app/components/setup/model/slider';
import { environment } from 'src/environments/environment';
import { CreateSliderComponent } from '../create-slider/create-slider.component';


@Component({
  selector: 'app-list-slider',
  templateUrl: './list-slider.component.html',
  styleUrls: ['./list-slider.component.css']
})
export class ListSliderComponent implements OnInit {

  spinnerName = 'listSpinner';
  sliders: Slider[];
  fileUrl = environment.fileUrl;
  bsModalRef: BsModalRef;

  constructor(
    private sliderService: SliderService,
    private alertify: AlertifyService,
    private spinner: SpinnerService,
    private modalService: BsModalService) {

   }




  ngOnInit() {
    this.getSliders();
  }

  getSliders() {
    this.spinner.showSpinner(this.spinnerName);
    this.sliderService.getSliders().subscribe((slider: Slider[]) => {
      this.sliders = slider;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load slider');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  deleteSlider(id: number) {
    this.alertify.confirm('Are you sure you want to delete this slider?', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.sliderService.deleteSlider(id).subscribe(() => {
        this.alertify.success('Slider deleted successfully!');
        this.getSliders();
        this.spinner.hideSpinner(this.spinnerName);
      }, error => {
        this.alertify.error('Failed to delete slider!');
        this.spinner.hideSpinner(this.spinnerName);
      });
    });
  }

  createSlider() {
    this.bsModalRef = this.modalService.show(CreateSliderComponent,  {
      initialState: {
        title: 'Create HomePage Slider'
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.sliderToCreate.subscribe((slider: Slider) => {
      const sliderToCreate = slider;
      if (sliderToCreate) {
        sliderToCreate.id = 0;
        this.sliderService.postSlider(sliderToCreate).subscribe(() => {
          this.alertify.success('Slider create successfully!');
          this.getSliders();
        }, error => {
          this.alertify.error('Failed to create slider!');
          console.log(error);
        });
      }
    });
  }

  updateSlider(slider: Slider) {
    this.bsModalRef = this.modalService.show(CreateSliderComponent,  {
      initialState: {
        title: 'Update HomePage Slider',
        slider
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.sliderToCreate.subscribe((sliderToUpdate: Slider) => {
      const sliderToCreate = sliderToUpdate;
      if (sliderToCreate) {
        sliderToCreate.id = slider.id;
        this.sliderService.postSlider(sliderToCreate).subscribe(() => {
          this.alertify.success('Slider update successfully!');
          this.getSliders();
        }, error => {
          this.alertify.error('Failed to update slider!');
          console.log(error);
        });
      }
    });
  }

}
