import { Component, OnInit } from '@angular/core';
import { PaymentTypeService } from 'src/app/components/setup/service/paymentType.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { PaymentType } from 'src/app/components/setup/model/paymentType';
import { environment } from 'src/environments/environment';
import { CreatePaymentTypeComponent } from '../create-payment-type/create-payment-type.component';

@Component({
  selector: 'app-list-payment-type',
  templateUrl: './list-payment-type.component.html',
  styleUrls: ['./list-payment-type.component.css']
})
export class ListPaymentTypeComponent implements OnInit {
  spinnerName = 'listSpinner';
  paymentTypes: PaymentType[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;

  constructor(private paymentTypeService: PaymentTypeService,
              private alertify: AlertifyService,
              private router: Router,
              private spinner: SpinnerService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.loadPaymentType();
  }

  loadPaymentType() {
    this.spinner.showSpinner(this.spinnerName);
    this.paymentTypeService.getPaymentTypes().subscribe((data: PaymentType[]) => {
      this.paymentTypes = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load payment type !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createPaymentType() {
    this.bsModalRef = this.modalService.show(CreatePaymentTypeComponent,  {
      initialState: {
        title: 'Create/Update Payment Type',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.paymentTypeToCreate.subscribe((paymentType: PaymentType) => {
      const paymentTypeToCreate = paymentType;
      if (paymentTypeToCreate) {
        paymentTypeToCreate.id = 0;
        this.paymentTypeService.createPaymentType(paymentTypeToCreate).subscribe(() => {
          this.alertify.success('payment type create successfully');
          this.loadPaymentType();
        }, error => {
          this.alertify.error('Failed to create payment type');
          console.log(error);
        });
      }
    });
  }

  editPaymentType(paymentType: PaymentType) {
    this.bsModalRef = this.modalService.show(CreatePaymentTypeComponent,  {
      initialState: {
        title: 'Create/Update Payment Type',
        paymentType
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.paymentTypeToCreate.subscribe((paymentTypeToUpdate: PaymentType) => {
      const paymentTypeToCreate = paymentTypeToUpdate;
      if (paymentTypeToCreate) {
        paymentTypeToCreate.id = paymentType.id;
        this.paymentTypeService.updatePaymentType(paymentTypeToCreate).subscribe(() => {
          this.alertify.success('payment type update successfully');
          this.loadPaymentType();
        }, error => {
          this.alertify.error('Failed to update payment type');
          console.log(error);
        });
      }
    });
  }

  deletePaymentType(id: number) {
    this.alertify.confirm('Are you sure you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.paymentTypeService.deletePaymentType(id).subscribe(() => {
        this.alertify.success('item delete successfully');
        this.loadPaymentType();
      }, error => {
        this.alertify.error('Failed to delete item');
        console.log(error);
      });
    });
  }

}
