import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentType } from 'src/app/components/setup/model/paymentType';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-payment-ype',
  templateUrl: './create-payment-type.component.html',
  styleUrls: ['./create-payment-type.component.css']
})
export class CreatePaymentTypeComponent implements OnInit {

  paymentTypeForm: FormGroup;
  paymentType: PaymentType;
  spinnerName = 'createSpinner';
  title: string;
  fileUrl = environment.fileUrl;
  @Output() paymentTypeToCreate = new EventEmitter();
  @ViewChild('fileInput', {static: false}) fileInputVariable: ElementRef;

  constructor(private fb: FormBuilder,
              private spinner: SpinnerService,
              public bsModalRef: BsModalRef,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.createPaymentTypeForm();
  }

  createPaymentTypeForm() {
    if (this.paymentType) {
      this.paymentTypeForm = this.fb.group({
        id: [this.paymentType.id],
        name: [this.paymentType.name, Validators.required],
        displayOrder: [this.paymentType.displayOrder, Validators.required],
        imageFile: [null]
      });
    } else {
      this.paymentTypeForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required],
        displayOrder: ['', Validators.required],
        imageFile: [null, Validators.required]
      });
    }
  }

  onSubmit(): void {
    console.log(this.paymentTypeForm.value);
    const formModel = this.prepareSave();
    this.paymentTypeToCreate.emit(formModel);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.paymentTypeForm.reset();
    this.fileInputVariable.nativeElement.value = '';
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.paymentTypeForm.get('imageFile').setValue(file);
      console.log('type', file.type);
    }
  }

  private prepareSave(): any {
    const input = new FormData();
    input.append('id', this.paymentTypeForm.get('id').value);
    input.append('name', this.paymentTypeForm.get('name').value);
    input.append('displayOrder', this.paymentTypeForm.get('displayOrder').value);
    input.append('imageFile', this.paymentTypeForm.get('imageFile').value);
    return input;
  }

}
