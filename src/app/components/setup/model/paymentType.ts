import { FileUploader } from 'ng2-file-upload';

export interface PaymentType {
    id: number;
    name: string;
    imagePath: string;
    imageFile?: File;
    displayOrder: number;
}