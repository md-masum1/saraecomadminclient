export interface Banner {
    id: number;
    title: string;
    description: string;
    navigateUrl: string;
    displayOrder: number;
    imagePath: string;
    imageFile?: File;
}