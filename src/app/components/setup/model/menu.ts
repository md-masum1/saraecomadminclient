export interface Menu {
    id: number;
    displayOrder: number;
    path?: string;
    title?: string;
    type?: string;
    megaMenu?: boolean;
    megaMenuType?: string; // small, medium, large
    subMenus: SubMenu[];
}

export interface SubMenu {
    id: number;
    displayOrder: number;
    path?: string;
    title?: string;
    type?: string;
    megaMenu?: boolean;
    megaMenuType?: string; // small, medium, large
    menuId: number;
    subSubMenus: SubSubMenu[];
}

export interface SubSubMenu {
    id: number;
    displayOrder: number;
    path?: string;
    title?: string;
    type?: string;
    megaMenu?: boolean;
    megaMenuType?: string; // small, medium, large
    subMenuId: number;
}

