import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Banner } from 'src/app/components/setup/model/banner';

@Injectable({
  providedIn: 'root'
})
export class BannerService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getBanners() {
    return this.http.get(this.baseUrl + 'setup/banner');
  }

  // getSlider(id: number) {
  //   return this.http.get(this.baseUrl + 'setup/banner/' + id);
  // }

  postBanner(banner: Banner) {
    return this.http.post(this.baseUrl + 'setup/banner', banner);
  }

  deleteBanner(id: number) {
    return this.http.delete(this.baseUrl + 'setup/banner/' + id);
  }
}
