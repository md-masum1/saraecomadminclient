import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Size } from 'src/app/components/setup/model/size';

@Injectable({
  providedIn: 'root'
})
export class SizeService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getSizes() {
    return this.http.get(this.baseUrl + 'setup/size');
  }

  createSize(size: Size) {
    return this.http.post(this.baseUrl + 'setup/size', size);
  }

  updateSize(size: Size) {
    return this.http.post(this.baseUrl + 'setup/size/update', size);
  }

  deleteSize(id: number) {
    return this.http.delete(this.baseUrl + 'setup/size/' + id);
  }
}
