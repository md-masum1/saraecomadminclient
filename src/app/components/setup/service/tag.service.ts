import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Tag } from 'src/app/components/setup/model/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getTags() {
    return this.http.get(this.baseUrl + 'setup/tag');
  }

  createTag(tag: Tag) {
    return this.http.post(this.baseUrl + 'setup/tag', tag);
  }

  updateTag(tag: Tag) {
    return this.http.post(this.baseUrl + 'setup/tag/update', tag);
  }

  deleteTag(id: number) {
    return this.http.delete(this.baseUrl + 'setup/tag/' + id);
  }
}
