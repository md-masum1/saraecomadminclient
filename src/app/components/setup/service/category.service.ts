import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Category, SubCategory } from 'src/app/components/setup/model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get(this.baseUrl + 'setup/category');
  }

  postCategory(category: Category) {
    return this.http.post(this.baseUrl + 'setup/category/createCategory', category);
  }

  deleteCategory(id: number) {
    return this.http.delete(this.baseUrl + 'setup/category/deleteCategory/' + id);
  }

  getSubCategories() {
    return this.http.get(this.baseUrl + 'setup/subcategory');
  }

  postSubCategory(subCategory: SubCategory) {
    return this.http.post(this.baseUrl + 'setup/category/createSubCategory', subCategory);
  }

  deleteSubCategory(id: number) {
    return this.http.delete(this.baseUrl + 'setup/category/deleteSubCategory/' + id);
  }

}
