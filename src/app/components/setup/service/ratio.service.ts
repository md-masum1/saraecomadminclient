import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Ratio } from 'src/app/components/setup/model/ratio';

@Injectable({
  providedIn: 'root'
})
export class RatioService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getRatios() {
    return this.http.get(this.baseUrl + 'setup/ratio');
  }

  createRatio(ratio: Ratio) {
    return this.http.post(this.baseUrl + 'setup/ratio', ratio);
  }

  updateRatio(ratio: Ratio) {
    return this.http.post(this.baseUrl + 'setup/ratio/update', ratio);
  }

  deleteRatio(id: number) {
    return this.http.delete(this.baseUrl + 'setup/ratio/' + id);
  }
}
