import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { PaymentType } from 'src/app/components/setup/model/paymentType';

@Injectable({
  providedIn: 'root'
})
export class PaymentTypeService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getPaymentTypes() {
    return this.http.get(this.baseUrl + 'setup/paymentType');
  }

  createPaymentType(paymentType: PaymentType) {
    return this.http.post(this.baseUrl + 'setup/paymentType', paymentType);
  }

  updatePaymentType(paymentType: PaymentType) {
    return this.http.post(this.baseUrl + 'setup/paymentType/update', paymentType);
  }

  deletePaymentType(id: number) {
    return this.http.delete(this.baseUrl + 'setup/paymentType/' + id);
  }
}
