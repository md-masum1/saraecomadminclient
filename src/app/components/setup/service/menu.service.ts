import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Menu, SubMenu, SubSubMenu } from 'src/app/components/setup/model/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

getMenu() {
  return this.http.get(this.baseUrl + 'setup/menu/getMenu');
}

getSubMenu() {
  return this.http.get(this.baseUrl + 'setup/menu/getSubMenu');
}

getSubSubMenu() {
  return this.http.get(this.baseUrl + 'setup/menu/getSubSubMenu');
}

createMenu(menu: Menu) {
  return this.http.post(this.baseUrl + 'setup/menu/createMenu', menu);
}

createSubMenu(subMenu: SubMenu) {
  return this.http.post(this.baseUrl + 'setup/menu/createSubMenu', subMenu);
}

createSubSubMenu(subSubMenu: SubSubMenu) {
  return this.http.post(this.baseUrl + 'setup/menu/createSubSubMenu', subSubMenu);
}

deleteMenu(id: number) {
  return this.http.delete(this.baseUrl + 'setup/menu/deleteMenu/' + id);
}

deleteSubMenu(id: number, menuId: number) {
  return this.http.delete(this.baseUrl + 'setup/menu/deleteSubMenu/' + id + '/' + menuId);
}

deleteSubSubMenu(id: number, subMenuId: number) {
  return this.http.delete(this.baseUrl + 'setup/menu/deleteSubSubMenu/' + id + '/' + subMenuId);
}

}
