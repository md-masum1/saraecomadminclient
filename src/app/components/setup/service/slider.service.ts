import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Slider } from 'src/app/components/setup/model/slider';

@Injectable({
  providedIn: 'root'
})
export class SliderService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getSliders() {
    return this.http.get(this.baseUrl + 'setup/slider');
  }

  getSlider(id: number) {
    return this.http.get(this.baseUrl + 'setup/slider/' + id);
  }

  postSlider(slider: Slider) {
    return this.http.post(this.baseUrl + 'setup/slider', slider);
  }

  deleteSlider(id: number) {
    return this.http.delete(this.baseUrl + 'setup/slider/' + id);
  }

}
