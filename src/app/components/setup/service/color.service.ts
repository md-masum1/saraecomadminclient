import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Color } from 'src/app/components/setup/model/color';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getColors() {
    return this.http.get(this.baseUrl + 'setup/color');
  }

  createColor(color: Color) {
    return this.http.post(this.baseUrl + 'setup/color', color);
  }

  updateColor(color: Color) {
    return this.http.post(this.baseUrl + 'setup/color', color);
  }

  deleteColor(id: number) {
    return this.http.delete(this.baseUrl + 'setup/color/' + id);
  }
}
