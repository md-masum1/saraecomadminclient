import { Component, OnInit } from '@angular/core';
import { SizeService } from 'src/app/components/setup/service/size.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Size } from 'src/app/components/setup/model/size';
import { environment } from 'src/environments/environment';
import { CreateSizeComponent } from '../create-size/create-size.component';

@Component({
  selector: 'app-list-size',
  templateUrl: './list-size.component.html',
  styleUrls: ['./list-size.component.css']
})
export class ListSizeComponent implements OnInit {

  spinnerName = 'listSpinner';
  sizes: Size[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;

  constructor(private sizeService: SizeService,
              private alertify: AlertifyService,
              private router: Router,
              private spinner: SpinnerService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.loadSize();
  }

  loadSize() {
    this.spinner.showSpinner(this.spinnerName);
    this.sizeService.getSizes().subscribe((data: Size[]) => {
      this.sizes = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load size !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createSize() {
    this.bsModalRef = this.modalService.show(CreateSizeComponent,  {
      initialState: {
        title: 'Create/Update Size',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.sizeToCreate.subscribe((size: Size) => {
      const sizeToCreate = size;
      if (sizeToCreate) {
        sizeToCreate.id = 0;
        this.sizeService.createSize(sizeToCreate).subscribe(() => {
          this.alertify.success('Size created successfully');
          this.loadSize();
        }, error => {
          this.alertify.error('Failed to create size!');
          console.log(error);
        });
      }
    });
  }

  editSize(size: Size) {
    this.bsModalRef = this.modalService.show(CreateSizeComponent,  {
      initialState: {
        title: 'Create/Update Size',
        size
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.sizeToCreate.subscribe((sizeToUpdate: Size) => {
      const sizeToCreate = sizeToUpdate;
      if (sizeToCreate) {
        sizeToCreate.id = size.id;
        this.sizeService.updateSize(sizeToCreate).subscribe(() => {
          this.alertify.success('size updated successfully!');
          this.loadSize();
        }, error => {
          this.alertify.error('Failed to update size!');
          console.log(error);
        });
      }
    });
  }

  deleteSize(id: number) {
    this.alertify.confirm('Are you sure you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.sizeService.deleteSize(id).subscribe(() => {
        this.alertify.success('item deleted successfully');
        this.loadSize();
      }, error => {
        this.alertify.error('Failed to delete item');
        console.log(error);
      });
    });
  }

}
