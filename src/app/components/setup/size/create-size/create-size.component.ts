import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Size } from 'src/app/components/setup/model/size';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';

@Component({
  selector: 'app-create-size',
  templateUrl: './create-size.component.html',
  styleUrls: ['./create-size.component.css']
})
export class CreateSizeComponent implements OnInit {

  sizeForm: FormGroup;
  size: Size;
  spinnerName = 'createSpinner';
  title: string;
  @Output() sizeToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.createSizeForm();
  }

  createSizeForm() {
    if (this.size) {
      this.sizeForm = this.fb.group({
        id: [this.size.id],
        name: [this.size.name, Validators.required]
      });
    } else {
      this.sizeForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.sizeToCreate.emit(this.sizeForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.sizeForm.reset();
  }
}
