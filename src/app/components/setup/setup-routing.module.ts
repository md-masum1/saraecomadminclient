import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListSliderComponent } from './slider/list-slider/list-slider.component';
import { CreateSliderComponent } from './slider/create-slider/create-slider.component';
import { ListBannerComponent } from './banner/list-banner/list-banner.component';
import { CreateBannerComponent } from './banner/create-banner/create-banner.component';
import { ListPaymentTypeComponent } from './payment-type/list-payment-type/list-payment-type.component';
import { CreatePaymentTypeComponent } from './payment-type/create-payment-type/create-payment-type.component';
import { ListCategoryComponent } from './category/list-category/list-category.component';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { ListColorComponent } from './color/list-color/list-color.component';
import { CreateColorComponent } from './color/create-color/create-color.component';
import { ListSizeComponent } from './size/list-size/list-size.component';
import { CreateSizeComponent } from './size/create-size/create-size.component';
import { ListTagComponent } from './tag/list-tag/list-tag.component';
import { CreateTagComponent } from './tag/create-tag/create-tag.component';
import { ListRatioComponent } from './ratio/list-ratio/list-ratio.component';
import { CreateRatioComponent } from './ratio/create-ratio/create-ratio.component';


const routes: Routes = [
  {
    path: '',
    children: [
        {
            path: 'slider/list-slider',
            component: ListSliderComponent,
            data: {
              title: 'Slider List',
              breadcrumb: 'Slider'
            }
          },

          {
            path: 'slider/create-slider',
            component: CreateSliderComponent,
            data: {
              title: 'Create Slider',
              breadcrumb: 'Slider'
            }
          },

      {
        path: 'banner/list-banner',
        component: ListBannerComponent,
        data: {
          title: 'Banner List',
          breadcrumb: 'Banner'
        }
      },

      {
        path: 'banner/create-banner',
        component: CreateBannerComponent,
        data: {
          title: 'Create Banner',
          breadcrumb: 'Banner'
        }
      },

      {
        path: 'payment-type/list-payment-type',
        component: ListPaymentTypeComponent,
        data: {
          title: 'Payment Type List',
          breadcrumb: 'Payment Type'
        }
      },

      {
        path: 'payment-type/create-payment-type',
        component: CreatePaymentTypeComponent,
        data: {
          title: 'Create Payment Type',
          breadcrumb: 'Payment Type'
        }
      },
      {
        path: 'category/list-category',
        component: ListCategoryComponent,
        data: {
          title: 'Category List',
          breadcrumb: 'Category'
        }
      },
      {
        path: 'category/create-category',
        component: CreateCategoryComponent,
        data: {
          title: 'Create Category',
          breadcrumb: 'Category'
        }
      },
      {
        path: 'color/list-color',
        component: ListColorComponent,
        data: {
          title: 'List Color',
          breadcrumb: 'Color'
        }
      },
      {
        path: 'color/create-color',
        component: CreateColorComponent,
        data: {
          title: 'Create Color',
          breadcrumb: 'Color'
        }
      },
      {
        path: 'size/list-size',
        component: ListSizeComponent,
        data: {
          title: 'List Size',
          breadcrumb: 'Size'
        }
      },
      {
        path: 'size/create-size',
        component: CreateSizeComponent,
        data: {
          title: 'Create Size',
          breadcrumb: 'Size'
        }
      },
      {
        path: 'tag/list-tag',
        component: ListTagComponent,
        data: {
          title: 'List Tag',
          breadcrumb: 'Tag'
        }
      },
      {
        path: 'tag/create-tag',
        component: CreateTagComponent,
        data: {
          title: 'Create Tag',
          breadcrumb: 'Tag'
        }
      },
      {
        path: 'ratio/list-ratio',
        component: ListRatioComponent,
        data: {
          title: 'List Ratio',
          breadcrumb: 'Ratio'
        }
      },
      {
        path: 'ratio/create-ratio',
        component: CreateRatioComponent,
        data: {
          title: 'Create Ratio',
          breadcrumb: 'Ratio'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }