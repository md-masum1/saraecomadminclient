import { NgModule } from '@angular/core';
import { SetupRoutingModule } from './setup-routing.module';
import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { ColorPickerModule } from 'ngx-color-picker';

import { SharedModule } from '../../shared/shared.module';

import { ListSliderComponent } from './slider/list-slider/list-slider.component';
import { CreateSliderComponent } from './slider/create-slider/create-slider.component';
import { ListBannerComponent } from './banner/list-banner/list-banner.component';
import { CreateBannerComponent } from './banner/create-banner/create-banner.component';
import { ListPaymentTypeComponent } from './payment-type/list-payment-type/list-payment-type.component';
import { CreatePaymentTypeComponent } from './payment-type/create-payment-type/create-payment-type.component';
import { ListCategoryComponent } from './category/list-category/list-category.component';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { CreateSubCategoryComponent } from './category/create-sub-category/create-sub-category.component';
import { ListColorComponent } from './color/list-color/list-color.component';
import { CreateColorComponent } from './color/create-color/create-color.component';
import { ListSizeComponent } from './size/list-size/list-size.component';
import { CreateSizeComponent } from './size/create-size/create-size.component';
import { ListTagComponent } from './tag/list-tag/list-tag.component';
import { CreateTagComponent } from './tag/create-tag/create-tag.component';
import { ListRatioComponent } from './ratio/list-ratio/list-ratio.component';
import { CreateRatioComponent } from './ratio/create-ratio/create-ratio.component';

@NgModule({
  imports: [
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    SetupRoutingModule,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    ListSliderComponent,
    CreateSliderComponent,
    ListBannerComponent,
    CreateBannerComponent,
    ListPaymentTypeComponent,
    CreatePaymentTypeComponent,
    ListCategoryComponent,
    CreateCategoryComponent,
    CreateSubCategoryComponent,
    ListColorComponent,
    CreateColorComponent,
    ListSizeComponent,
    CreateSizeComponent,
    ListTagComponent,
    CreateTagComponent,
    ListRatioComponent,
    CreateRatioComponent
  ],
  entryComponents: [
    CreateSliderComponent,
    CreateBannerComponent,
    CreatePaymentTypeComponent,
    CreateCategoryComponent,
    CreateSubCategoryComponent,
    CreateColorComponent,
    CreateSizeComponent,
    CreateTagComponent,
    CreateRatioComponent
  ],
  providers: []
})
export class SetupModule { }
