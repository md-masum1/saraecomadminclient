import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Category } from 'src/app/components/setup/model/category';
import { BsModalRef } from 'ngx-bootstrap';


@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {
  categoryForm: FormGroup;
  category: Category;
  spinnerName = 'createSpinner';
  title: string;
  @Output() categoryToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
    public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createCategoryForm();
  }
  createCategoryForm() {
    if (this.category) {
      this.categoryForm = this.fb.group({
        id: [this.category.id],
        name: [this.category.name, Validators.required]
      });
    } else {
      this.categoryForm = this.fb.group({
        id: [0],
        name: [null, Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.categoryToCreate.emit(this.categoryForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.categoryForm.reset();
  }
}
