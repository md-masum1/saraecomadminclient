import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SubCategory } from 'src/app/components/setup/model/category';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-sub-category',
  templateUrl: './create-sub-category.component.html',
  styleUrls: ['./create-sub-category.component.css']
})
export class CreateSubCategoryComponent implements OnInit {

  subCategoryForm: FormGroup;
  subCategory: SubCategory;
  spinnerName = 'createSpinner';
  name: string;
  title: string;
  categoryId: number;

  @Output() subCategoryToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createSubCategoryForm();
  }

  createSubCategoryForm() {
    if (this.subCategory) {
      this.subCategoryForm = this.fb.group({
        id: [this.subCategory.id],
        name: [this.subCategory.name, Validators.required],
        categoryId: [this.subCategory.categoryId, Validators.required]
      });
    } else {
      this.subCategoryForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required],
        categoryId: [this.categoryId, Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.subCategoryToCreate.emit(this.subCategoryForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.subCategoryForm.reset();
  }

}
