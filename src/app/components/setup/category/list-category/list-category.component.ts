import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/components/setup/service/category.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { Category, SubCategory } from 'src/app/components/setup/model/category';

import { environment } from 'src/environments/environment';
import { CreateCategoryComponent } from '../create-category/create-category.component';
import { CreateSubCategoryComponent } from '../create-sub-category/create-sub-category.component';


@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {

  spinnerName = 'listSpinner';
  categories: Category[];
  fileUrl = environment.fileUrl;
  bsModalRef: BsModalRef;
  showCategoryId = 0;
  showSubCategoryId = 0;
  show = false;

  constructor(
    private categoryService: CategoryService,
    private alertify: AlertifyService,
    private spinner: SpinnerService,
    private modalService: BsModalService) {

   }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.spinner.showSpinner(this.spinnerName);
    this.categoryService.getCategories().subscribe((category: Category[]) => {
      this.categories = category;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load category!');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  deleteCategory(id: number) {
    this.alertify.confirm('Are you sure you want to delete this category?', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.categoryService.deleteCategory(id).subscribe(() => {
        this.alertify.success('Category deleted successfully!');
        this.getCategories();
        this.spinner.hideSpinner(this.spinnerName);
      }, error => {
        this.alertify.error('Failed to delete category!');
        this.spinner.hideSpinner(this.spinnerName);
      });
    });
  }

  createCategory() {
    this.bsModalRef = this.modalService.show(CreateCategoryComponent,  {
      initialState: {
        title: 'Create Category'
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.categoryToCreate.subscribe((category: Category) => {
      const categoryToCreate = category;
      if (categoryToCreate) {
        categoryToCreate.id = 0;
        this.categoryService.postCategory(categoryToCreate).subscribe(() => {
          this.alertify.success('Category created successfully!');
          this.getCategories();
        }, error => {
          this.alertify.error('Failed to create category!');
          console.log(error);
        });
      }
    });
  }



  updateCategory(category: Category) {
    this.bsModalRef = this.modalService.show(CreateCategoryComponent,  {
      initialState: {
        title: 'Update Category',
        category
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.categoryToCreate.subscribe((categoryToUpdate: Category) => {
      const categoryToCreate = categoryToUpdate;
      if (categoryToCreate) {
        categoryToCreate.id = category.id;
        this.categoryService.postCategory(categoryToCreate).subscribe(() => {
          this.alertify.success('Category update successfully!');
          this.getCategories();
        }, error => {
          this.alertify.error('Failed to update category!');
          console.log(error);
        });
      }
    });
  }

  createSubCategory(categoryId: number) {
    this.bsModalRef = this.modalService.show(CreateSubCategoryComponent, {
      initialState: {
        title: 'Create sub Category',
        categoryId
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subCategoryToCreate.subscribe((subcategory: SubCategory) => {
      const subcategoryToCreate = subcategory;
      if (subcategoryToCreate) {
        subcategoryToCreate.id = 0;
        this.categoryService.postSubCategory(subcategoryToCreate).subscribe(() => {
          this.alertify.success('Sub category created successfully!');
          this.getCategories();
        }, error => {
          this.alertify.error('Failed to create sub category!');
          console.log(error);
        });
      }
    });
  }

  updateSubCategory(subCategory: SubCategory, categoryId: number) {

    subCategory.categoryId = categoryId;

    this.bsModalRef = this.modalService.show(CreateSubCategoryComponent, {
      initialState: {
        title: 'Update sub Category',
        subCategory
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subCategoryToCreate.subscribe((subcategory: SubCategory) => {
      const subcategoryToCreate = subcategory;
      if (subcategoryToCreate) {
        subcategoryToCreate.id = subCategory.id;
        this.categoryService.postSubCategory(subcategoryToCreate).subscribe(() => {
          this.alertify.success('Sub category update successfully!');
          this.getCategories();
        }, error => {
          this.alertify.error('Failed to update sub category!');
          console.log(error);
        });
      }
    });
  }

  deleteSubCategory(id: number) {
    this.alertify.confirm('Are you sure you want to delete this sub category?', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.categoryService.deleteSubCategory(id).subscribe(() => {
        this.alertify.success('Sub category deleted successfully!');
        this.getCategories();
      }, error => {
        this.alertify.error('Failed to delete sub category!');
        console.log(error);
      });
    });
  }

  showSubCategory(id: number) {
    if (this.showCategoryId === id) {
      this.show = !this.show;
    } else {
      this.show = true;
    }
    this.showCategoryId = id;
  }



}
