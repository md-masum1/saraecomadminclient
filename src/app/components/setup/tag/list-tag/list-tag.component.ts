import { Component, OnInit } from '@angular/core';
import { TagService } from 'src/app/components/setup/service/tag.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Tag } from 'src/app/components/setup/model/tag';
import { environment } from 'src/environments/environment';
import { CreateTagComponent } from '../create-tag/create-tag.component';

@Component({
  selector: 'app-list-tag',
  templateUrl: './list-tag.component.html',
  styleUrls: ['./list-tag.component.css']
})
export class ListTagComponent implements OnInit {

  spinnerName = 'listSpinner';
  tags: Tag[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;

  constructor(private tagService: TagService,
              private alertify: AlertifyService,
              private router: Router,
              private spinner: SpinnerService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.loadTag();
  }

  loadTag() {
    this.spinner.showSpinner(this.spinnerName);
    this.tagService.getTags().subscribe((data: Tag[]) => {
      this.tags = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load tags !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createTag() {
    this.bsModalRef = this.modalService.show(CreateTagComponent,  {
      initialState: {
        title: 'Create/Update Tag',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.tagToCreate.subscribe((tag: Tag) => {
      const tagToCreate = tag;
      if (tagToCreate) {
        tagToCreate.id = 0;
        this.tagService.createTag(tagToCreate).subscribe(() => {
          this.alertify.success('Tag created successfully');
          this.loadTag();
        }, error => {
          this.alertify.error('Failed to create tag!');
          console.log(error);
        });
      }
    });
  }

  editTag(tag: Tag) {
    this.bsModalRef = this.modalService.show(CreateTagComponent,  {
      initialState: {
        title: 'Create/Update Tag',
        tag
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.tagToCreate.subscribe((tagToUpdate: Tag) => {
      const tagToCreate = tagToUpdate;
      if (tagToCreate) {
        tagToCreate.id = tag.id;
        this.tagService.updateTag(tagToCreate).subscribe(() => {
          this.alertify.success('Tag updated successfully!');
          this.loadTag();
        }, error => {
          this.alertify.error('Failed to update tag!');
          console.log(error);
        });
      }
    });
  }

  deleteTag(id: number) {
    this.alertify.confirm('Are you sure you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.tagService.deleteTag(id).subscribe(() => {
        this.alertify.success('item deleted successfully');
        this.loadTag();
      }, error => {
        this.alertify.error('Failed to delete item');
        console.log(error);
      });
    });
  }
}
