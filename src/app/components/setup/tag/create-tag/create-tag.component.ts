import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Tag } from 'src/app/components/setup/model/tag';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';

@Component({
  selector: 'app-create-tag',
  templateUrl: './create-tag.component.html',
  styleUrls: ['./create-tag.component.css']
})
export class CreateTagComponent implements OnInit {
  tagForm: FormGroup;
  tag: Tag;
  spinnerName = 'createSpinner';
  title: string;
  @Output() tagToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.createTagForm();
  }

  createTagForm() {
    if (this.tag) {
      this.tagForm = this.fb.group({
        id: [this.tag.id],
        name: [this.tag.name, Validators.required]
      });
    } else {
      this.tagForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.tagToCreate.emit(this.tagForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.tagForm.reset();
  }
}
