import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Color } from 'src/app/components/setup/model/color';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';


@Component({
  selector: 'app-create-color',
  templateUrl: './create-color.component.html',
  styleUrls: ['./create-color.component.css']
})
export class CreateColorComponent implements OnInit {

  colorForm: FormGroup;
  color: Color;
  colorCode = '';
  spinnerName = 'createSpinner';
  title: string;
  @Output() colorToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              public bsModalRef: BsModalRef,
              private alertify: AlertifyService) { }

  ngOnInit() {
    this.createColorForm();
  }

  onColorCodeChange(colorCode: string) {
    this.colorForm.get('colorCode').setValue(colorCode);
  }

  createColorForm() {
    if (this.color) {
      this.colorCode = this.color.colorCode;
      this.colorForm = this.fb.group({
        id: [this.color.id],
        name: [this.color.name, Validators.required],
        colorCode: [this.color.colorCode, Validators.required]
      });
    } else {
      this.colorForm = this.fb.group({
        id: ['0'],
        name: ['', Validators.required],
        colorCode: ['', Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.colorToCreate.emit(this.colorForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.colorForm.reset();
  }




}
