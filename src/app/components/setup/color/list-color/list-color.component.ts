import { Component, OnInit } from '@angular/core';
import { ColorService } from 'src/app/components/setup/service/color.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Color } from 'src/app/components/setup/model/color';
import { environment } from 'src/environments/environment';
import { CreateColorComponent } from '../create-color/create-color.component';

@Component({
  selector: 'app-list-color',
  templateUrl: './list-color.component.html',
  styleUrls: ['./list-color.component.css']
})
export class ListColorComponent implements OnInit {

  spinnerName = 'listSpinner';
  colors: Color[];
  bsModalRef: BsModalRef;
  fileUrl = environment.fileUrl;

  constructor(private colorService: ColorService,
              private alertify: AlertifyService,
              private router: Router,
              private spinner: SpinnerService,
              private modalService: BsModalService) { }

  ngOnInit() {
    this.loadColor();
  }

  loadColor() {
    this.spinner.showSpinner(this.spinnerName);
    this.colorService.getColors().subscribe((data: Color[]) => {
      this.colors = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load color !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createColor() {
    this.bsModalRef = this.modalService.show(CreateColorComponent,  {
      initialState: {
        title: 'Create/Update Color',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.colorToCreate.subscribe((color: Color) => {
      const colorToCreate = color;
      if (colorToCreate) {
        colorToCreate.id = 0;
        this.colorService.createColor(colorToCreate).subscribe(() => {
          this.alertify.success('Color create successfully');
          this.loadColor();
        }, error => {
          this.alertify.error('Failed to create color');
          console.log(error);
        });
      }
    });
  }

  editColor(color: Color) {
    this.bsModalRef = this.modalService.show(CreateColorComponent,  {
      initialState: {
        title: 'Create/Update Color',
        color
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.colorToCreate.subscribe((colorToUpdate: Color) => {
      const colorToCreate = colorToUpdate;
      if (colorToCreate) {
        colorToCreate.id = color.id;
        this.colorService.updateColor(colorToCreate).subscribe(() => {
          this.alertify.success('color updated successfully!');
          this.loadColor();
        }, error => {
          this.alertify.error('Failed to update color!');
          console.log(error);
        });
      }
    });
  }

  deleteColor(id: number) {
    this.alertify.confirm('Are you sure you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.colorService.deleteColor(id).subscribe(() => {
        this.alertify.success('item deleted successfully');
        this.loadColor();
      }, error => {
        this.alertify.error('Failed to delete item');
        console.log(error);
      });
    });
  }


}
