import { NgModule } from '@angular/core';

import { CKEditorModule } from 'ngx-ckeditor';
import 'hammerjs';
import 'mousetrap';

import { ProductsRoutingModule } from './products-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ListProductComponent } from './list-product/list-product.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { AddProductImageComponent } from './create-product/add-product-image/add-product-image.component';
import { CreateColorComponent } from './create-product/create-color/create-color.component';
import { CreateSizeComponent } from './create-product/create-size/create-size.component';
import { TabsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    ListProductComponent,
    CreateProductComponent,
    AddProductImageComponent,
    CreateColorComponent,
    CreateSizeComponent
  ],
  imports: [
    CKEditorModule,
    ProductsRoutingModule,
    SharedModule,
    TabsModule.forRoot()
  ],
  providers: []
})
export class ProductsModule { }
