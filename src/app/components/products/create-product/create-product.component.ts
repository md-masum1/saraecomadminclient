import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { Product, ProductPicture, ProductColor, ProductSize } from '../model/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { CategoryService } from 'src/app/components/setup/service/category.service';
import { Category, SubCategory } from 'src/app/components/setup/model/category';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Color } from '../../setup/model/color';
import { ColorService } from '../../setup/service/color.service';
import { Size } from '../../setup/model/size';
import { SizeService } from '../../setup/service/size.service';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap';
import { AddProductImageComponent } from './add-product-image/add-product-image.component';
import { CreateColorComponent } from './create-color/create-color.component';
import { CreateSizeComponent } from './create-size/create-size.component';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit  {
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;
  @ViewChild(AddProductImageComponent) productImage!: AddProductImageComponent;
  @ViewChild(CreateColorComponent) createColor: CreateColorComponent;
  @ViewChild(CreateSizeComponent) createSize: CreateSizeComponent;

  productForm: FormGroup;
  product: Product;
  categoryList: Category[];
  subCategoryList: SubCategory[];
  spinnerName = 'createSpinner';
  @Output() menuToCreate = new EventEmitter();

  active = 1;

  baseUrl = environment.apiUrl;
  fileUrl = environment.fileUrl;

  constructor(private fb: FormBuilder,
    private spinner: SpinnerService,
    private categoryService: CategoryService,
    private sizeService: SizeService,
    private alertify: AlertifyService,
    private productService: ProductService,
    private router: Router) { }

  ngOnInit() {
    const data = history.state;
    if('id' in data) {
      this.product = data;
    }
    this.createProductForm();
    this.loadCaregory();
  }

  onSelect(data: TabDirective): void {
    if(data.heading === 'Images') {
      this.productImage.isActive = true;
      this.productImage.imagePanel();
    }
    if(data.heading === 'Color') {
      this.createColor.isActive = true;
      this.createColor.colorPanel();
    }
    if(data.heading === 'Size') {
      this.createSize.isActive = true;
      this.createSize.sizePanel();
    }
  }

  // General Setup
  createProductForm() {
    if(this.product) {
      this.productForm = this.fb.group({
        id: [this.product.id],
        whProductId: [this.product.whProductId],
        name: [this.product.name, Validators.required],
        productStyle: [this.product.productStyle, Validators.required],
        isActive: [this.product.isActive, Validators.required],
        shortDescription: [this.product.shortDescription, Validators.required],
        longDescription: [this.product.longDescription, Validators.required],
        fabric: [this.product.fabric, Validators.required],
        categoryId: [this.product.categoryId, Validators.required],
        subCategoryId: [this.product.subCategoryId, Validators.required],
        new: [this.product.new, Validators.required],
        sale: [this.product.sale, Validators.required],
        price: [this.product.price, Validators.required],
        salePrice: [this.product.salePrice, Validators.required],
        discount: [this.product.discount, Validators.required],
        stock: [this.product.stock, Validators.required]
      });
    } else {
      this.productForm = this.fb.group({
        id: [0],
        whProductId: [0],
        name: [null, Validators.required],
        productStyle: [null, Validators.required],
        isActive: [true, Validators.required],
        shortDescription: [null, Validators.required],
        longDescription: [null, Validators.required],
        fabric: [null, Validators.required],
        categoryId: [null, Validators.required],
        subCategoryId: [null, Validators.required],
        new: [true, Validators.required],
        sale: [false, Validators.required],
        price: [null, Validators.required],
        salePrice: [null, Validators.required],
        discount: [null, Validators.required],
        stock: [null, Validators.required]
      });
    }
  }

  loadCaregory() {
    this.spinner.showSpinner(this.spinnerName);
    this.categoryService.getCategories().subscribe((data: Category[]) => {
      this.spinner.hideSpinner(this.spinnerName);
      this.categoryList = data;
      if(this.product) {
        this.onSelectCategory(this.product.categoryId);
      }
    }, error => {
      console.log(error);
      this.alertify.error('Failed to load payment type !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  onSelectCategory(id: number) {
    let categoryId = this.productForm.get('categoryId').value;
    if(id) {
      categoryId = id;
    }

    if(categoryId) {
      const parent = this.categoryList.filter(c => c.id === categoryId);
      parent.filter(s => {
        this.subCategoryList = s.subCategories;
      });
    } else {
      this.productForm.get('categoryId').setValue(categoryId);
      this.subCategoryList = null;
    }
  }

  onSubmit() {
    this.spinner.showSpinner(this.spinnerName);
    this.productService.createProduct(this.productForm.value).subscribe(() => {
      if(this.product) {
        this.alertify.success('product update successfully');
      } else {
        this.alertify.success('product create successfully');
      }
      this.productService.getProductByStyle(this.productForm.get('productStyle').value).subscribe((data: Product) => {
        this.product = data;
        this.createProductForm();
      }, error => {
        this.spinner.hideSpinner(this.spinnerName);
        this.alertify.error('Failed to load product');
        this.router.navigate(['/products/']);
      });
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to create product');
      this.spinner.hideSpinner(this.spinnerName);
      console.log(error);
    });
  }

  onClear() {
    this.createProductForm();
  }
  // End General Setup
}
