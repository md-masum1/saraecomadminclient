import { Component, OnInit, Input } from '@angular/core';
import { Product, ProductSize } from '../../model/product';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SizeService } from 'src/app/components/setup/service/size.service';
import { Size } from 'src/app/components/setup/model/size';

@Component({
  selector: 'app-create-size',
  templateUrl: './create-size.component.html',
  styleUrls: ['./create-size.component.css']
})
export class CreateSizeComponent implements OnInit {
  @Input() productFromCreateProduct: Product;
  product: Product;
  spinnerName = 'createSpinner';

  isActive = false;

  productSizeForm: FormGroup;
  sizeList: Size[];
  productSizes: ProductSize[];
  productSize: ProductSize;
  productSizeArray: any[];

  constructor(private spinner: SpinnerService,
    private fb: FormBuilder,
    private productService: ProductService,
    private alertify: AlertifyService,
    private sizeService: SizeService) { }

  ngOnInit() {
    this.product = this.productFromCreateProduct;
    console.log(this.product);
  }

  // start size setup

  sizePanel() {
    if(this.product) {
      this.getSizeList();
      this.getProductSize();

      if(this.productSize) {
        this.productSizeForm = this.fb.group({
          id: [this.productSize.id],
          sizeId: [this.productSize.sizeId, Validators.required],
          colorId: [this.productSize.colorId],
          productId: [this.productSize.productId, Validators.required],
          stock: [this.productSize.stock],
          price: [this.productSize.price]
        });
      } else {
        this.productSizeForm = this.fb.group({
          id: [0],
          sizeId: [null, Validators.required],
          colorId: [null, Validators.required],
          productId: [this.product.id],
          stock: [null],
          price: [null]
        });
      }
    }
  }

  getSizeList() {
    this.spinner.showSpinner(this.spinnerName);
    this.sizeService.getSizes().subscribe((data: Size[]) => {
      this.spinner.hideSpinner(this.spinnerName);
      this.sizeList = data;
    }, error => {
      console.log(error);
      this.alertify.error('Failed to load size !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  getProductSize() {
    const id = this.product.id;
    this.spinner.showSpinner(this.spinnerName);
    this.productService.getProductSize(id).subscribe((data: ProductSize[]) => {
      this.spinner.hideSpinner(this.spinnerName);
      this.productSizes = data;

      const groups = this.productSizes.reduce((r, a) => {
          r[a.colorName] = r[a.colorName] || [];
          r[a.colorName].push(a);
          return r;
      }, {});

    this.productSizeArray = Object.keys(groups).map((key) => {
        return {color: key, productSize: groups[key]};
    });
    }, error => {
      console.log(error);
      this.alertify.error('Failed to load size !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  deleteProductSize(id: number) {
    this.alertify.confirm('Are you sure you want to delete this size???', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.productService.deleteProductSize(id).subscribe(() => {
        this.spinner.hideSpinner(this.spinnerName);
        this.alertify.success('product size delete successfully');
        this.getProductSize();
      }, error => {
        console.log(error);
        this.alertify.error('Failed to delete product size!');
        this.spinner.hideSpinner(this.spinnerName);
      });
    });
  }

  // end size setup
}
