import { Component, OnInit, Input } from '@angular/core';
import { ProductPicture, Product } from '../../model/product';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { ProductService } from '../../services/product.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';

@Component({
  selector: 'app-add-product-image',
  templateUrl: './add-product-image.component.html',
  styleUrls: ['./add-product-image.component.css']
})
export class AddProductImageComponent implements OnInit {
  @Input() productFromCreateProduct: Product;
  product: Product;
  productImage: ProductPicture[];
  uploader: FileUploader;
  hasBaseDropZoneOver = false;
  baseUrl = environment.apiUrl;
  fileUrl = environment.fileUrl;

  spinnerName = 'createSpinner';

  isActive = false;

  constructor(private spinner: SpinnerService,
            private productService: ProductService,
            private alertify: AlertifyService,) { }

  ngOnInit() {
    this.product = this.productFromCreateProduct;
    console.log(this.product);
  }

  // Image Setup
  imagePanel() {
    console.log(this.product);
    if(this.product) {
      this.getProductImage();
      this.initializeUploader();
    }
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  getProductImage() {
    this.spinner.showSpinner(this.spinnerName);
    this.productService.getProductImage(this.product.id).subscribe((photo: ProductPicture[]) => {
      this.productImage = photo;
      this.spinner.hideSpinner(this.spinnerName);
    });
    this.spinner.hideSpinner(this.spinnerName);
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + 'Product/uploadImage/' + this.product.id,
      authToken: 'Bearer ' + localStorage.getItem('token'),
      isHTML5: true,
      allowedFileType: ['image'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024
    });

    this.uploader.onAfterAddingFile = (file) => {file.withCredentials = false; };

    this.uploader.onSuccessItem = (item, response, ststus, headers) => {
      if (ststus === 201) {
        this.getProductImage();
      }
    };
  }

  deleteImage(imageId: number, productId: number) {
    this.alertify.confirm('Are you sure to delete this image???', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.productService.deleteProductImage(imageId, productId).subscribe(() => {
        this.spinner.hideSpinner(this.spinnerName);
        this.alertify.success('product image delete successfully');
        this.getProductImage();
      }, error => {
        console.log(error);
        this.alertify.error('Failed to delete product image!');
        this.spinner.hideSpinner(this.spinnerName);
      });
    })
  }
  // End Image Setup

}
