import { Component, OnInit, Input } from '@angular/core';
import { Product, ProductColor } from '../../model/product';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { ProductService } from '../../services/product.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Color } from 'src/app/components/setup/model/color';
import { environment } from 'src/environments/environment';
import { ColorService } from 'src/app/components/setup/service/color.service';

@Component({
  selector: 'app-create-color',
  templateUrl: './create-color.component.html',
  styleUrls: ['./create-color.component.css']
})
export class CreateColorComponent implements OnInit {
  @Input() productFromCreateProduct: Product;
  product: Product;
  spinnerName = 'createSpinner';

  productColorForm: FormGroup;
  colorList: Color[];
  productColors: ProductColor[];
  productColor: ProductColor;

  baseUrl = environment.apiUrl;
  fileUrl = environment.fileUrl;

  isActive = false;

  constructor(private spinner: SpinnerService,
            private fb: FormBuilder,
            private productService: ProductService,
            private alertify: AlertifyService,
            private colorService: ColorService) { }

  ngOnInit() {
    this.product = this.productFromCreateProduct;
    console.log(this.product);
  }

  // start color setup
  colorPanel() {
    if(this.product) {
      this.getColorList();
      this.getProductColor();

      if(this.productColor) {
        this.productColorForm = this.fb.group({
          id: [this.productColor.id],
          colorId: [this.productColor.colorId, Validators.required],
          productId: [this.productColor.productId],
          imageFile: [null, Validators.required]
        });
      } else {
        this.productColorForm = this.fb.group({
          id: [0],
          colorId: [null, Validators.required],
          productId: [this.product.id],
          imageFile: [null, Validators.required]
        });
      }
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.productColorForm.get('imageFile').setValue(file);
      console.log('type', file.type);
    }
  }

  onColorSubmit() {
    this.spinner.showSpinner(this.spinnerName);
    const productColor = this.prepareSave();
    this.productService.addProductColor(productColor).subscribe(() => {
      this.spinner.hideSpinner(this.spinnerName);
      this.alertify.success('color add successfully');
      this.onColorClear();
    }, error => {
      this.alertify.error('Failed to add color');
      this.spinner.hideSpinner(this.spinnerName);
      console.log(error);
    });
  }

  private prepareSave(): any {
    const input = new FormData();
    input.append('id', this.productColorForm.get('id').value);
    input.append('colorId', this.productColorForm.get('colorId').value);
    input.append('productId', this.productColorForm.get('productId').value);
    input.append('imageFile', this.productColorForm.get('imageFile').value);
    return input;
  }

  onColorClear() {
    this.productColor = null;
    this.colorPanel();
  }

  getProductColor() {
    const id = this.product.id;
    this.spinner.showSpinner(this.spinnerName);
    this.productService.getProductColor(id).subscribe((data: ProductColor[]) => {
      this.spinner.hideSpinner(this.spinnerName);
      this.productColors = data;
    }, error => {
      console.log(error);
      this.alertify.error('Failed to load color !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  deleteProductColor(colorId: number, productId: number) {
    this.alertify.confirm('Are you sure you want to delete this color???', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.productService.deleteProductColor(colorId, productId).subscribe(() => {
        this.spinner.hideSpinner(this.spinnerName);
        this.alertify.success('product color delete successfully');
        this.getProductColor();
      }, error => {
        console.log(error);
        this.alertify.error('Failed to delete product color!');
        this.spinner.hideSpinner(this.spinnerName);
      });
    });
  }

  updateProductColor(productColor: ProductColor) {
    this.productColor = productColor;
    this.colorPanel();
  }

  getColorList() {
    this.spinner.showSpinner(this.spinnerName);
    this.colorService.getColors().subscribe((data: Color[]) => {
      this.spinner.hideSpinner(this.spinnerName);
      this.colorList = data;
    }, error => {
      console.log(error);
      this.alertify.error('Failed to load color !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }
  // end color setup
}
