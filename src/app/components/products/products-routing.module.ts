import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductComponent } from './list-product/list-product.component';
import { CreateProductComponent } from './create-product/create-product.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ListProductComponent,
        data: {
          title: 'List Product',
          breadcrumb: 'List Product'
        }
      },
      {
        path: 'add',
        component: CreateProductComponent,
        data: {
          title: 'Add Product',
          breadcrumb: 'Add Product'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
