import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Product, ProductColor, ProductSize } from '../model/product';
import { Observable } from 'rxjs';
import { PaginatedResult } from 'src/app/_models/pagination';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

createProduct(product: Product) {
  return this.http.post(this.baseUrl + 'product/createProduct/', product);
}

getProductByStyle(style: string) {
  return this.http.get(this.baseUrl + 'product/getProduct/' + style);
}

getAllProduct(page?, itemsPerPage?, productParams?): Observable<PaginatedResult<Product[]>> {
  const paginatedResult: PaginatedResult<Product[]> = new PaginatedResult<Product[]>();

  let params = new HttpParams();

  if (page != null && itemsPerPage != null) {
    params = params.append('pageNumber', page);
    params = params.append('pageSize', itemsPerPage);
  }

  if (productParams != null) {
    params = params.append('savedProduct', productParams.savedProduct);
    params = params.append('style', productParams.style);
    params = params.append('categoryName', productParams.categoryName);
    params = params.append('subCategoryName', productParams.subCategoryName);
  }

  return this.http.get<Product[]>(this.baseUrl + 'product/getProduct', { observe: 'response', params })
  .pipe(
    map(response => {
      paginatedResult.result = response.body;
      if (response.headers.get('Pagination') != null) {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
      }
      return paginatedResult;
    })
  );
}

getProductImage(id: number) {
  return this.http.get(this.baseUrl + 'product/GetProductPhotos/' + id);
}

deleteProductImage(imageId: number, productId: number) {
  return this.http.delete(this.baseUrl + 'product/deleteImage/' + imageId + '/' + productId);
}

addProductColor(productColor: ProductColor) {
  return this.http.post(this.baseUrl + 'product/addProductColor', productColor);
}

getProductColor(id: number) {
  return this.http.get(this.baseUrl + 'product/getProductColor/' + id);
}

deleteProductColor(colorId: number, productId: number) {
  return this.http.delete(this.baseUrl + 'product/deleteProductColor/' + colorId + '/' + productId);
}

getProductSize(id: number) {
  return this.http.get(this.baseUrl + 'product/getProductSize/' + id);
}

addProductSize(productSize: ProductSize) {
  return this.http.post(this.baseUrl + 'product/addProductSize', productSize);
}

deleteProductSize(id: number) {
  return this.http.delete(this.baseUrl + 'product/deleteProductSize/' + id);
}
}
