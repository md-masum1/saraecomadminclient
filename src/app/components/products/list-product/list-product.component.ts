import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';
import { ProductService } from '../services/product.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { Pagination, PaginatedResult } from 'src/app/_models/pagination';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  productList: Product[];
  spinnerName = 'listSpinner';
  productParams: any = {};
  pagination: Pagination;
  currentPage = 1;
  maxSize = 10;

  constructor(private productService: ProductService,
    private alertify: AlertifyService,
    private router: Router,
    private spinner: SpinnerService) { }

  ngOnInit() {
    this.loadProduct();
  }

  loadProduct() {
    this.spinner.showSpinner(this.spinnerName);
    this.productService.getAllProduct().subscribe((data: PaginatedResult<Product[]>) => {
      this.productList = data.result;
      this.pagination = data.pagination;
      this.currentPage = this.pagination.currentPage;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load product !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  viewProduct(product: Product) {

  }

  createProduct() {
    this.router.navigate(['/products/add']);
  }

  editProduct(product: Product) {
    this.router.navigateByUrl('/products/add', { state: product });
  }

  deleteProduct(id: number) {

  }

}
