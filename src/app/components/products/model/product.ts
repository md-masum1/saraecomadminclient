export interface Product {
    id: number;
    whProductId: number;
    name: string;
    productStyle: string;
    isActive: boolean;
    shortDescription: string;
    longDescription: string;
    fabric: string;
    category: ProductCategory;
    categoryId: number;
    subCategory: ProductSubCategory;
    subCategoryId: number;
    new: boolean;
    sale: boolean;
    price: number;
    salePrice: number;
    discount: number;
    stock: number;
    productColors: ProductColor[];
    productSizes: ProductSize[];
    productTags: ProductTag[];
    pictures: ProductPicture[];
}

export interface ProductCategory {
    id: number;
    name: string;
}

export interface ProductSubCategory {
    id: number;
    name: string;
    categoryId: number;
}

export interface ProductColor {
    id: number;
    colorId: number;
    colorName: string;
    productId: number;
    images: string;
    imageFile?: File;
}

export interface ProductSize {
    id: number;
    productId: number;

    sizeId: number;
    sizeName?: string;

    colorId?: number;
    colorName?: string;

    stock?: number;
    price?: number;
}

export interface ProductTag {
    tagId: number;
    productId: number;
}

export interface ProductPicture {
    id: number;
    image: string;
    imageFile?: File;
    productId: number;
}