import { NgModule } from '@angular/core';
import { WebDataRocksPivot } from '../../webdatarocks/webdatarocks.angular4';
import { ModalModule, PaginationModule } from 'ngx-bootstrap';

import { SharedModule } from '../../shared/shared.module';

import { MenusRoutingModule } from './menus-routing.module';
import { ListMenuComponent } from './list-menu/list-menu.component';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { CreateSubMenuComponent } from './create-sub-menu/create-sub-menu.component';
import { CreateSubSubMenuComponent } from './create-sub-sub-menu/create-sub-sub-menu.component';

@NgModule({
  declarations: [
    ListMenuComponent,
    CreateMenuComponent,
    CreateSubMenuComponent,
    CreateSubSubMenuComponent,
    WebDataRocksPivot
  ],
  imports: [
    MenusRoutingModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    SharedModule
  ],
  entryComponents: [
    CreateMenuComponent,
    CreateSubMenuComponent,
    CreateSubSubMenuComponent
  ],
})
export class MenusModule { }
