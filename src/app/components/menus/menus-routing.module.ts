import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMenuComponent } from './list-menu/list-menu.component';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { CreateSubMenuComponent } from './create-sub-menu/create-sub-menu.component';
import { CreateSubSubMenuComponent } from './create-sub-sub-menu/create-sub-sub-menu.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list-menu',
        component: ListMenuComponent,
        data: {
          title: 'Menu Lists',
          breadcrumb: 'Menu Lists'
        }
      },
      {
        path: 'create-menu',
        component: CreateMenuComponent,
        data: {
          title: 'Create Menu',
          breadcrumb: 'Create Menu'
        }
      },
      {
        path: 'create-sub-menu',
        component: CreateSubMenuComponent,
        data: {
          title: 'Create Sub Menu',
          breadcrumb: 'Create Sub Menu'
        }
      },
      {
        path: 'create-sub-sub-menu',
        component: CreateSubSubMenuComponent,
        data: {
          title: 'Create Sub Sub Menu',
          breadcrumb: 'Create Sub Sub Menu'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenusRoutingModule { }
