import { Component, OnInit, ViewChild } from '@angular/core';
import { Menu, SubMenu, SubSubMenu } from 'src/app/components/setup/model/menu';
import { MenuService } from 'src/app/components/setup/service/menu.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { CreateMenuComponent } from '../create-menu/create-menu.component';
import { CreateSubMenuComponent } from '../create-sub-menu/create-sub-menu.component';
import { CreateSubSubMenuComponent } from '../create-sub-sub-menu/create-sub-sub-menu.component';
import { WebDataRocksPivot } from 'src/app/webdatarocks/webdatarocks.angular4';

@Component({
  selector: 'app-list-menu',
  templateUrl: './list-menu.component.html',
  styleUrls: ['./list-menu.component.scss']
})
export class ListMenuComponent implements OnInit {

  spinnerName = 'listSpinner';
  menus: Menu[];
  showMenuId = 0;
  show = false;
  showSubMenuId = 0;
  showSubSub = false;
  bsModalRef: BsModalRef;

  constructor(private menuService: MenuService,
    private alertify: AlertifyService,
    private router: Router,
    private spinner: SpinnerService,
    private modalService: BsModalService) { }


    @ViewChild('pivot') child: WebDataRocksPivot;

  onPivotReady(pivot: WebDataRocks.Pivot): void {
    console.log('[ready] WebDataRocksPivot', this.child);
  }

  onReportComplete(): void {
    this.child.webDataRocks.off('reportcomplete');
    this.child.webDataRocks.setReport({
      dataSource: {
        data: this.menus
      }
    });
  }

  ngOnInit() {
    this.loadMenu();
  }

  loadMenu() {
    this.spinner.showSpinner(this.spinnerName);
    this.menuService.getMenu().subscribe((data: Menu[]) => {
     this.menus = data;
     this.spinner.hideSpinner(this.spinnerName);
     console.log(data);
   }, error => {
     this.alertify.error(error);
     this.spinner.hideSpinner(this.spinnerName);
   });
  }

  onSubMenuShowClick(menuId: number) {
    this.showMenuId = menuId;
    this.show = !this.show;
  }

  onSubSubMenuShowClick(subMenuId: number) {
    this.showSubMenuId = subMenuId;
    this.showSubSub = !this.showSubSub;
  }

  onCreateMenu() {
    this.bsModalRef = this.modalService.show(CreateMenuComponent,  {
      initialState: {
        title: 'Create Menu',
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.menuToCreate.subscribe((menu: Menu) => {
      const menuToCreate = menu;
      console.log(menuToCreate);
      if (menuToCreate) {
        menuToCreate.id = 0;
        this.menuService.createMenu(menuToCreate).subscribe(() => {
          this.alertify.success('Menu created successfully !');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to create menu !');
          console.log(error);
        });
      }
    });
  }

  onEditMenu(menu: Menu) {
    this.bsModalRef = this.modalService.show(CreateMenuComponent,  {
      initialState: {
        title: 'Modal title',
        menu
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.menuToCreate.subscribe((menuToUpdate: Menu) => {
      const menuToCreate = menuToUpdate;
      if (menuToCreate) {
        menuToCreate.id = menu.id;
        this.menuService.createMenu(menuToCreate).subscribe(() => {
          this.alertify.success('menu update successfully');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to update menu');
          console.log(error);
        });
      }
    });
  }

  onEditSubMenu(subMenu: SubMenu, menuName: string) {
    this.bsModalRef = this.modalService.show(CreateSubMenuComponent,  {
      initialState: {
        title: 'Edit Sub Menu For: ' + menuName,
        subMenu
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subMenuToCreate.subscribe((subMenutoUpdate: SubMenu) => {
      const subMenuToCreate = subMenutoUpdate;
      if (subMenuToCreate) {
        subMenuToCreate.id = subMenu.id;
        subMenuToCreate.menuId = subMenu.menuId;
        console.log(subMenuToCreate);
        this.menuService.createSubMenu(subMenuToCreate).subscribe(() => {
          this.alertify.success('sub-menu update successfully');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to update sub-menu');
          console.log(error);
        });
      }
    });
  }

  onEditSubSubMenu(subSubMenu: SubSubMenu, subMenuName: string) {
    this.bsModalRef = this.modalService.show(CreateSubSubMenuComponent,  {
      initialState: {
        title: 'Edit Sub Sub Menu For: ' + subMenuName,
        subSubMenu
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subSubMenuToCreate.subscribe((subSubMenuToEdit: SubSubMenu) => {
      const subSubMenuToCreate = subSubMenuToEdit;
      if (subSubMenuToCreate) {
        subSubMenuToCreate.id = subSubMenu.id;
        subSubMenuToCreate.subMenuId = subSubMenu.subMenuId;
        console.log(subSubMenuToCreate);
        this.menuService.createSubSubMenu(subSubMenuToCreate).subscribe(() => {
          this.alertify.success('sub-sub-menu update successfully');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to update sub-sub-menu');
          console.log(error);
        });
      }
    });
  }

  onDeleteMenu(id: number) {
    this.alertify.confirm('Are you sure you want to delete this menu', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.menuService.deleteMenu(id).subscribe(() => {
        this.alertify.success('Menu delete successfully');
        this.loadMenu();
      }, error => {
        this.alertify.error('Failed to delete menu');
        console.log(error);
      });
    });
  }

  onDeleteSubMenu(id: number, menuId: number) {
    this.alertify.confirm('Are you sure you want to delete this sub menu', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.menuService.deleteSubMenu(id, menuId).subscribe(() => {
        this.alertify.success('sub menu delete successfully');
        this.loadMenu();
      }, error => {
        this.alertify.error('Failed to delete sub menu');
        console.log(error);
      });
    });
  }

  onDeleteSubSubMenu(id: number, subMenuId: number) {
    this.alertify.confirm('Are you sure you want to delete this sub-sub menu', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.menuService.deleteSubSubMenu(id, subMenuId).subscribe(() => {
        this.alertify.success('sub-sub menu delete successfully');
        this.loadMenu();
      }, error => {
        this.alertify.error('Failed to delete sub-sub menu');
        console.log(error);
      });
    });
  }

  onCreateSubMenu(menuName: string, menuId: number) {
    this.bsModalRef = this.modalService.show(CreateSubMenuComponent,  {
      initialState: {
        title: 'Create Sub Menu For: ' + menuName,
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subMenuToCreate.subscribe((subMenu: SubMenu) => {
      const subMenuToCreate = subMenu;
      if (subMenuToCreate) {
        subMenuToCreate.id = 0;
        subMenuToCreate.menuId = menuId;
        this.menuService.createSubMenu(subMenuToCreate).subscribe(() => {
          this.alertify.success('sub-menu create successfully');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to create sub-menu');
          console.log(error);
        });
      }
    });
  }

  onCreateSubSubMenu(subMenuName: string, subMenuId: number) {
    this.bsModalRef = this.modalService.show(CreateSubSubMenuComponent,  {
      initialState: {
        title: 'Create Sub Sub Menu For: ' + subMenuName,
        data: {}
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.subSubMenuToCreate.subscribe((subSubMenu: SubSubMenu) => {
      const subSubMenuToCreate = subSubMenu;
      if (subSubMenuToCreate) {
        subSubMenuToCreate.id = 0;
        subSubMenuToCreate.subMenuId = subMenuId;
        console.log(subSubMenuToCreate);
        this.menuService.createSubSubMenu(subSubMenuToCreate).subscribe(() => {
          this.alertify.success('sub-sub-menu create successfully');
          this.loadMenu();
        }, error => {
          this.alertify.error('Failed to create sub-sub-menu');
          console.log(error);
        });
      }
    });
  }

}
