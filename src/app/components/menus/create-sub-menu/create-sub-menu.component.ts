import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SubMenu } from 'src/app/components/setup/model/menu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-sub-menu',
  templateUrl: './create-sub-menu.component.html',
  styleUrls: ['./create-sub-menu.component.css']
})
export class CreateSubMenuComponent implements OnInit {
  subMenuForm: FormGroup;
  subMenu: SubMenu;
  spinnerName = 'createSpinner';
  title: string;
  @Output() subMenuToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              private spinner: SpinnerService,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createSubMenuForm();
  }

  createSubMenuForm() {
    if (this.subMenu) {
      this.subMenuForm = this.fb.group({
        id: [this.subMenu.id],
        displayOrder: [this.subMenu.displayOrder, Validators.required],
        path: [this.subMenu.path],
        title: [this.subMenu.title, Validators.required],
        type: [this.subMenu.type],
        megaMenu: [this.subMenu.megaMenu],
        megaMenuType: [this.subMenu.megaMenuType]
      });
    } else {
      this.subMenuForm = this.fb.group({
        id: ['0'],
        displayOrder: ['', Validators.required],
        path: [''],
        title: ['', Validators.required],
        type: [''],
        megaMenu: [true],
        megaMenuType: ['']
      });
    }
  }

  onSubmit(): void {
    this.subMenuToCreate.emit(this.subMenuForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.subMenuForm.reset();
  }

}
