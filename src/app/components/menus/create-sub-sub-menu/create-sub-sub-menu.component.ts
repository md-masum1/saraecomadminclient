import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubSubMenu } from 'src/app/components/setup/model/menu';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-sub-sub-menu',
  templateUrl: './create-sub-sub-menu.component.html',
  styleUrls: ['./create-sub-sub-menu.component.css']
})
export class CreateSubSubMenuComponent implements OnInit {
  subSubMenuForm: FormGroup;
  subSubMenu: SubSubMenu;
  spinnerName = 'createSpinner';
  title: string;
  @Output() subSubMenuToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
              private spinner: SpinnerService,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    console.log(this.subSubMenu);
    this.createSubSubMenuForm();
  }

  createSubSubMenuForm() {
    if (this.subSubMenu) {
      this.subSubMenuForm = this.fb.group({
        id: [this.subSubMenu.id],
        title: [this.subSubMenu.title, Validators.required],
        displayOrder: [this.subSubMenu.displayOrder, Validators.required],
        subMenuId: [this.subSubMenu.subMenuId],
        path: [this.subSubMenu.path],
        type: [this.subSubMenu.type],
        megaMenu: [this.subSubMenu.megaMenu],
        megaMenuType: [this.subSubMenu.megaMenuType]

      });
    } else {
      this.subSubMenuForm = this.fb.group({
        id: ['0'],
        title: ['', Validators.required],
        displayOrder: ['', Validators.required],
        subMenuId: [''],
        path: [''],
        type: [''],
        megaMenu: [true],
        megaMenuType: ['']

      });
    }
  }

  onSubmit(): void {
    this.subSubMenuToCreate.emit(this.subSubMenuForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.subSubMenuForm.reset();
  }

}
