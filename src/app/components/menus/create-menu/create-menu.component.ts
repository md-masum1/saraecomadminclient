import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { BsModalRef } from 'ngx-bootstrap';
import { Menu } from 'src/app/components/setup/model/menu';

@Component({
  selector: 'app-create-menu',
  templateUrl: './create-menu.component.html',
  styleUrls: ['./create-menu.component.scss']
})
export class CreateMenuComponent implements OnInit {

  menuForm: FormGroup;
  menu: Menu;
  spinnerName = 'createSpinner';
  @Output() menuToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
    private spinner: SpinnerService,
    public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createMenuForm();
  }

  createMenuForm() {
    if (this.menu) {
      console.log(this.menu);
      this.menuForm = this.fb.group({
        id: [this.menu.id],
        displayOrder: [this.menu.displayOrder, Validators.required],
        path: [this.menu.path],
        title: [this.menu.title, Validators.required],
        type: [this.menu.type],
        megaMenu: [this.menu.megaMenu],
        megaMenuType: [this.menu.megaMenuType]
      });
    } else {
      this.menuForm = this.fb.group({
        id: ['0'],
        displayOrder: ['', Validators.required],
        path: [''],
        title: ['', Validators.required],
        type: [''],
        megaMenu: [true],
        megaMenuType: ['']
      });
    }
  }

  onSubmit(): void {
    this.menuToCreate.emit(this.menuForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.menuForm.reset();
  }

}
