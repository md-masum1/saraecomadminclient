import { Injectable, HostListener, Inject } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { WINDOW } from './windows.service';
// Menu
export interface Menu {
  path?: string;
  title?: string;
  icon?: string;
  type?: string;
  badgeType?: string;
  badgeValue?: string;
  active?: boolean;
  bookmark?: boolean;
  children?: Menu[];
}

@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor(@Inject(WINDOW) private window) {
    this.onResize();
    if (this.screenWidth < 991) {
      this.collapseSidebar = true;
    }
  }
  public screenWidth: any;
  public collapseSidebar = false;

  MENUITEMS: Menu[] = [
    {
      path: '/dashboard/default',
      title: 'Dashboard',
      icon: 'home',
      type: 'link',
      badgeType: 'primary',
      active: false
    },

    {
      title: 'Menus',
      icon: 'align-left',
      type: 'sub',
      active: false,
      children: [
        { path: '/menus/list-menu', title: 'Menu Lists', type: 'link' }
        // { path: '/menus/create-menu', title: 'Create Menu', type: 'link' }
      ]
    },

    {
      title: 'Setup',
      icon: 'settings',
      type: 'sub',
      active: false,
      children: [
        { path: '/setup/slider/list-slider', title: 'Slider', type: 'link' },
        { path: '/setup/banner/list-banner', title: 'Banner', type: 'link' },
        { path: '/setup/payment-type/list-payment-type', title: 'Payment Type', type: 'link' },
        { path: '/setup/category/list-category', title: 'Category', type: 'link' },
        { path: '/setup/color/list-color', title: 'Color', type: 'link' },
        { path: '/setup/size/list-size', title: 'Size', type: 'link' },
        { path: '/setup/tag/list-tag', title: 'Tag', type: 'link' },
        { path: '/setup/ratio/list-ratio', title: 'Ratio', type: 'link' }
      ]
    },

    {
      title: 'Products',
      icon: 'box',
      type: 'sub',
      active: false,
      children: [
        {
          path: '/products',
          title: 'List Product',
          type: 'link'
        },
        {
          path: '/products/add',
          title: 'Add Product',
          type: 'link'
        },
      ]
    },

    {
      title: 'Admin',
      icon: 'user-plus',
      type: 'sub',
      active: false,
      children: [
        { path: '/admin/user-management/list-user', title: 'User Management', type: 'link' },
        { path: '/admin/role-management/list-user-roles', title: 'Role Management', type: 'link' }
      ]
    },
  ];
  // Array
  items = new BehaviorSubject<Menu[]>(this.MENUITEMS);

  // Windows width
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }
}
