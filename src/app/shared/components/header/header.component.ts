import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavService } from '../../service/nav.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public rightSidebar = false;
  public open = false;
  public openNav = false;
  public isOpenMobile : boolean;
  @Output() rightSidebarEvent = new EventEmitter<boolean>();

  constructor(public navServices: NavService, private alertify: AlertifyService, private router: Router) { }

  collapseSidebar() {
    this.open = !this.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar
  }
  right_side_bar() {
    this.rightSidebar = !this.rightSidebar
    this.rightSidebarEvent.emit(this.rightSidebar)
  }

  openMobileNav() {
    this.openNav = !this.openNav;
  }


  ngOnInit() {  }

  logOut() {
    localStorage.removeItem('token');
    this.alertify.success('loged out');
    this.router.navigate(['/login']);
}

}
